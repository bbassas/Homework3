//Homework 3 Dungeon
import java.util.Scanner;
public class Dungeon 

{
    private static final int NUMBER_OF_ROOMS = 5;
    private static final int NO_EXIT = -1;
    private static final int NORTH = 0;
    private static final int SOUTH = 1;
    private static final int EAST = 2;
    private static final int WEST = 3;
    private static int currentRoom;
    private static boolean systemRun;
    private static String userInput;
    private static int coordinates[][] = new int[NUMBER_OF_ROOMS][4];
    
    static String[] roomTitle = {"Florida Room","Kitchen","Living Room", "Bathroom", "Bedroom"};
    
    static String[] roomDescription = { "\nYou are in the Florida room. There are five garden gnomes staring directly at you.\nThere is a passage leading south.",
                                        "\nYou are in the kitchen. Grandma has some stew cooking. \nThere are passages leading east, south, north.",
                                        "\nYou are in the living room. The living room is filled with Elvis memorabilia. Grandma might be a horder.\nThere are passges leading west and south.",
                                        "\nYou are in the bathroom. There are 2 pairs of dentures in a glass. Grandma might have a lover.\nThere are passges leading north and west.",
                                        "\nYou are in the bedroom. It smells like moth balls and 5 cats are on the bed.\nThere are passages leading east and north."
                                      };
    
    
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        setUpCoordinates();
        currentRoom = 0;
        systemRun = true;
        System.out.println("\nYou are visting your wacky grandma in Florida. You are bored so you decide to look around her house.");
        
        while (systemRun == true)
        {
            System.out.println(printRoomDescription(currentRoom));
            System.out.println("Which way would you like to go?\nn: north\ns: south\nw: west\ne: east\nq: quit");
            userInput = input.next();
            executeUserChoice();
        }
        
            System.out.println("You've had enough of grandma's house and leave Florida.");   
        
    } //end main method
    public static void setUpCoordinates()
    {
        coordinates[0][1] = 1;
        coordinates[0][0] = -1;
        coordinates[0][2] = -1;
        coordinates[0][3] = -1;
        coordinates[1][2] = 2;
        coordinates[1][0] = 0;
        coordinates[1][1] = 4;
        coordinates[1][3] = -1;
        coordinates[2][1] = 3;
        coordinates[2][3] = 1;
        coordinates[2][2] = -1;
        coordinates[2][0] = -1;
        coordinates[3][3] = 4;
        coordinates[3][0] = 2;
        coordinates[3][1] = -1;
        coordinates[3][2] = -1;
        coordinates[4][0] = 1;
        coordinates[4][2] = 3;
        coordinates[4][1] = -1;
        coordinates[4][3] = -1;
       
    }
    
    public static void executeUserChoice()
    {
        int chosenDirection = NO_EXIT;
        if (userInput.equals("q") || userInput.equals("Q"))
        {
            systemRun = false;
        }
       else if (userInput.equals("n") || userInput.equals("N"))
       {
           chosenDirection = NORTH;
       }
       else if (userInput.equals("s") || userInput.equals("S"))
       {
            chosenDirection = SOUTH;
       }
       else if (userInput.equals("e") || userInput.equals("E"))
       {
           chosenDirection = EAST;
       }
       else if (userInput.equals("w") || userInput.equals("W"))
       {
            chosenDirection = WEST;
       }
       else
       {
           System.out.println("Invaild selection.");
       }
       
       if(chosenDirection != NO_EXIT)
       {
            int nextRoom = getRoomInDirection(currentRoom, chosenDirection);
       
            if (nextRoom != NO_EXIT)
            {
                currentRoom = nextRoom;
            }
            else
            {
                System.out.println("\nYou walk into a wall. You can't go that way.");
            }
       }
        
    }
    
    public static int getRoomInDirection(int room, int direction)
    {
        
       return coordinates[room][direction];
    }
    
    public static String printRoomDescription(int indexRoom)
    {
        
        return roomDescription[indexRoom];
    }

    
}